# Créé par pjacq, le 06/03/2022 en Python 3.7
import time, pygame
from pygame.locals import *

#initialisation de la vitesse de déplacement
vitesse = [2, 2]
# création d'une couleur (en RVB)
couleur_fond = 50, 50, 50
# initialisation des dimensions de la fenêtre pygame
dimension = largeur, hauteur = 600, 600

# ---------------- PYGAME -------------------
pygame.init()
# Création de la fenêtre
fenetre = pygame.display.set_mode(dimension)
# Création d'une surface pygame appelée balle contenant l'image
balle = pygame.image.load("Tennis-Ball.png")
# Création d'un rectangle contenant la surface
ballerect = balle.get_rect()

continuer = True
while continuer:

    # Déplacement de ballerect
    ballerect = ballerect.move(vitesse)
    if ballerect.left < 0 or ballerect.right > largeur:
        vitesse[0] = -vitesse[0]
    if ballerect.top < 0 or ballerect.bottom > hauteur:
        vitesse[1] = -vitesse[1]

    # On efface toute la fenêtre avec la couleur de fond
    fenetre.fill(couleur_fond)
    # On place la surface balle à l'emplacement de ballerect
    fenetre.blit(balle, ballerect)

    # Temporisation (en seconde)
    time.sleep(0.01)

    # Mise à jour de l'affichage
    pygame.display.update()

    # La boucle while s'arrête si la touche escape a été pressée
    for event in pygame.event.get():
        if event.type == KEYDOWN and event.key == K_ESCAPE:
            continuer = False

pygame.quit()